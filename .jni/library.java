/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "minorMatrix(int,int,double**,int,int):double**")
    public double[][] minorMatrix(int deleteRow, int deleteColumn, double[][] matrix, int matrixRows, int matrixColumns){
        return minorMatrix_(deleteRow, deleteColumn, matrix, matrixRows, matrixColumns);
    }
    private native double[][] minorMatrix_(int deleteRow, int deleteColumn, double[][] matrix, int matrixRows, int matrixColumns);

    @executerpane.MethodAnnotation(signature = "determinant(double**,int,int):double")
    public double determinant(double[][] matrix, int matrixRows, int matrixColumns){
        return determinant_(matrix, matrixRows, matrixColumns);
    }
    private native double determinant_(double[][] matrix, int matrixRows, int matrixColumns);

    @executerpane.MethodAnnotation(signature = "matrixWithReplacedColumn(int,double*,double**,int,int):double**")
    public double[][] matrixWithReplacedColumn(int columnIndex, double[] columnValue, double[][] matrix, int matrixRows, int matrixColumns){
        return matrixWithReplacedColumn_(columnIndex, columnValue, matrix, matrixRows, matrixColumns);
    }
    private native double[][] matrixWithReplacedColumn_(int columnIndex, double[] columnValue, double[][] matrix, int matrixRows, int matrixColumns);

    @executerpane.MethodAnnotation(signature = "cramerSystemUnknown(double**,int,int,double*):double*")
    public double[] cramerSystemUnknown(double[][] coeficient, int coeficientRows, int coeficientColumns, double[] independent){
        return cramerSystemUnknown_(coeficient, coeficientRows, coeficientColumns, independent);
    }
    private native double[] cramerSystemUnknown_(double[][] coeficient, int coeficientRows, int coeficientColumns, double[] independent);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
