#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

double** minorMatrix(int deleteRow, int deleteColumn, double** matrix, int matrixRows, int matrixColumns) {
    return NULL;
}

double determinant(double** matrix, int matrixRows, int matrixColumns) {
    return -1000.0;
}

double** matrixWithReplacedColumn(int columnIndex, double* columnValue, double** matrix, int matrixRows, int matrixColumns) {
    return NULL;
}

double* cramerSystemUnknown(double** coeficient, int coeficientRows, int coeficientColumns, double* independent) {
    return NULL;
}
