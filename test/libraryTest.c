#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

const double TOLERANCE = 1e-9;

void testMinorMatrix_A() {
    // Given
    double** matrix = (double**) malloc(2 * sizeof(double*));
	matrix[0] = (double*) malloc(2 * sizeof(double));
    matrix[0][0] = 1565.0;
    matrix[0][1] = 1921.0;
    matrix[1] = (double*) malloc(2 * sizeof(double));
    matrix[0][0] = 79.0;
    matrix[0][1] = -9.0;
    double** esperada = (double**) malloc(1 * sizeof(double*));
	esperada[0] = (double*) malloc(1 * sizeof(double));
	esperada[0][0] = 79.0;
    // When
    double** resultado = minorMatrix(1,0, matrix, 2, 2);
    // Then
    double** solucion = (double**) malloc(1 * sizeof(double*));
	solucion[0] = (double*) malloc(1 * sizeof(double));
    solucion[0][0] = 79.0;
    assertMatrixEquals_int(solucion, 1, 1, resultado,1 ,1);
}

void testMinorMatrix_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMinorMatrix_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMinorMatrix_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMinorMatrix_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testDeterminant_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMatrixWithReplacedColumn_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMatrixWithReplacedColumn_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMatrixWithReplacedColumn_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMatrixWithReplacedColumn_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testMatrixWithReplacedColumn_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testCramerSystemUnknown_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testCramerSystemUnknown_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testCramerSystemUnknown_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testCramerSystemUnknown_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testCramerSystemUnknown_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
